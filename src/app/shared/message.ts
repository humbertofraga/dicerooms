export interface Message {
    sent: number;
    author: string;
    content: string;
}
