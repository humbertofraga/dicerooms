export interface Member {
    user_id: string;
    display_name: string;
    password: string;
}
