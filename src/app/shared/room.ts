import { Message } from "./message";

export interface Room {
    id: string;
    password: string;
    owner: string;
}
