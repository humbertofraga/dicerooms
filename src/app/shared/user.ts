export interface User {
    uid: string;
    email: string;
    display_name: string;
    email_verified: boolean;
    last_activity: string;
}
