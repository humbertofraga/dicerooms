import { stringify } from '@angular/compiler/src/util';
import { Injectable, NgZone } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentData, DocumentReference, QueryFn, QuerySnapshot } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { map } from 'jquery';
import { Observable } from 'rxjs';
import { Member } from '../member';
import { Message } from '../message';
import { Room } from '../room';
import { User } from '../user';
import { AuthService } from './auth.service';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {
  private roomsCollection: AngularFirestoreCollection<Room>;

  constructor(
    public firestore: AngularFirestore,
    public authService: AuthService,
    public userService: UsersService,
    public router: Router,
    public zone: NgZone
  ) {
    this.roomsCollection = firestore.collection<Room>('rooms');
  }

  newRoom(password: string) {
    console.log("newRoom");
    const owner_id: string = this.authService.userData.uid;
    const room: Room = {
      id: '',
      password: password,
      owner: owner_id
    };
    this.roomsCollection.add(room)
      .then((result) => {
        console.log(`then ${result}`);
        this.zone.run(() => {
          this.router.navigate([`room/${result.id}`]);
        })
      })
      .catch((error) => {
        console.log(error.message);
      })
  }

  getRoom(room_id: string): AngularFirestoreDocument<Room> {
    return this.roomsCollection.doc<Room>(room_id);
  }

  getMessagesCollection(room_id: string, queryFn?: QueryFn<DocumentData> | undefined): AngularFirestoreCollection<Message> {
    return this.roomsCollection.doc(room_id).collection<Message>('/messages', queryFn);
  }

  getMembersCollection(room_id: string, queryFn?: QueryFn<DocumentData> | undefined): AngularFirestoreCollection<Member> {
    return this.roomsCollection.doc(room_id).collection('/members', queryFn);
  }

  addMember(room_id: string, user_id: string, password?: string|undefined) {
    this.getMembersCollection(room_id, ref => ref.where("user_id", "==", user_id))
        .valueChanges().subscribe((result) => {
      var membersMap = new Map<string, Member>(result.map(i => [i.user_id, i]));
      if (!membersMap.has(user_id)) {
        this.userService.getUser(user_id).valueChanges().subscribe((user) => {
          if (user != undefined) {
            this.getMembersCollection(room_id).doc(user.uid).set({
              user_id: user.uid,
              display_name: user.display_name,
              password: password? password : ""
            });
          }
        })
      }
    });
  }

  sendMessage(room_id: string, user_id: string, message: string) {
    const newMessage: Message = {
      sent: Date.now(),
      author: user_id,
      content: message
    };
    this.getMessagesCollection(room_id).add(newMessage);
  }

}
function ref(arg0: string, ref: any): AngularFirestoreCollection<import("firebase").default.firestore.DocumentData> {
  throw new Error('Function not implemented.');
}

function queryFn(arg0: string, queryFn: any): AngularFirestoreCollection<import("firebase").default.firestore.DocumentData> {
  throw new Error('Function not implemented.');
}

