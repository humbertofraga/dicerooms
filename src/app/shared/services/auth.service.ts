import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import firebase from 'firebase';
import { User } from '../user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData: any;

  constructor(
    public firestore: AngularFirestore,
    public fireauth: AngularFireAuth,
    public router: Router,
    public zone: NgZone
  ) {
    this.fireauth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
      } else {
        localStorage.setItem('user', '');
      }
    })
  }

  async signInAnonymously(path: string) {
    try {
      const result = await this.fireauth.signInAnonymously();
      this.zone.run(() => {
        this.router.navigate([path]);
      });
      this.setUserData(result.user);
    } catch (error) {
      console.log(error.message);
    }
  }

  async signIn(email: string, password: string, next: string|null) {
    try {
      const result = await this.fireauth.signInWithEmailAndPassword(email, password);
      this.setUserData(result.user);
      this.zone.run(() => {
        console.log(`routing to ${next}`);
        if (next !== null) {
          this.router.navigate([next]);
        } else {
          this.router.navigate(['welcome']);
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  async signUp(email: string, password: string) {
    try {
      const result = await this.fireauth.createUserWithEmailAndPassword(email, password)
      this.setUserData(result.user);
      this.zone.run(() => {
        this.router.navigate(['welcome'])
      })
    } catch (error) {
      console.log(error);
    }
  }

  googleAuth(next: string|null) {
    return this.authLogin(new firebase.auth.GoogleAuthProvider(), next);
  }

  async authLogin(provider: firebase.auth.AuthProvider, next: string|null) {
    try {
      const result = await this.fireauth.signInWithPopup(provider);
      this.setUserData(result.user);
      this.zone.run(() => {
        console.log(`routnig to ${next}`);
        if (next !== null) {
          this.router.navigate([next]);
        } else {
          this.router.navigate(['welcome']);
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  public async signOut() {
    await this.fireauth.signOut();
    localStorage.removeItem('user');
    this.router.navigate(['/sign-in']);
  }

  get isLoggedIn(): boolean {
    const lsUser = localStorage.getItem('user');
    if ((lsUser !== null) && (lsUser !== "")) {
      const user = JSON.parse(lsUser);
      return (user !== null)
    } else {
      return false
    }
  }

  setUserData(user: any) {
    const userRef: AngularFirestoreDocument<any> = this.firestore.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      display_name: user.displayName,
      last_activity: '',
      email_verified: false
    }
    return userRef.set(userData, { merge: true });
  }
}
