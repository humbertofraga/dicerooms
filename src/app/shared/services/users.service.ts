import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, QuerySnapshot } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { User } from '../user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private itemsCollection: AngularFirestoreCollection<User>;

  constructor(
    public firestore: AngularFirestore,
  ) {
    this.itemsCollection = firestore.collection<User>('users');
  }

  getUser(user_id: string): AngularFirestoreDocument<User> {
    return this.itemsCollection.doc<User>(user_id);
  }

  queryUsers(user_ids: string[]): Promise<QuerySnapshot<User>> {
    return this.itemsCollection.ref
        .where('uid', 'in', user_ids)
        .get();
  }
}
