import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { RoomsService } from 'src/app/shared/services/rooms.service';

@Component({
  selector: 'app-new-room',
  templateUrl: './new-room.component.html',
  styleUrls: ['./new-room.component.sass']
})
export class NewRoomComponent implements OnInit {

  constructor(
    public authService: AuthService,
    public roomsService: RoomsService
  ) {
  }

  ngOnInit(): void {
    
  }

}
