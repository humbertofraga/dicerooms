import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.sass']
})
export class SignInComponent implements OnInit {
  public next: string | null;

  constructor(
    public authService: AuthService,
    public route: ActivatedRoute,
    public router: Router,
    public zone: NgZone
  ) {
    this.next = this.route.snapshot.queryParamMap.get('next');
  }

  ngOnInit(): void {
    console.log(this.next);
    if (this.authService.isLoggedIn) {
      this.zone.run(() => {
        if (this.next !== null) {
          this.router.navigate([this.next]);
        } else {
          this.router.navigate(['welcome']);
        }
      })
    }
  }

}
