import { Component, OnInit } from '@angular/core';
import { RoomsService } from 'src/app/shared/services/rooms.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.sass']
})
export class WelcomeComponent implements OnInit {

  constructor(public roomsService: RoomsService) { }

  ngOnInit(): void {
  }

}
