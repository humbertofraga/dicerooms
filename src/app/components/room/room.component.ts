import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Room } from 'src/app/shared/room';
import { AuthService } from 'src/app/shared/services/auth.service';
import { RoomsService } from 'src/app/shared/services/rooms.service';
import { UsersService } from 'src/app/shared/services/users.service';
import { Message } from 'src/app/shared/message';
import { Member } from 'src/app/shared/member';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.sass']
})
export class RoomComponent implements OnInit, AfterViewInit {
  room: Room | undefined;
  messages: Observable<Message[]>;
  members: Observable<Member[]>;
  membersMap: Map<string, Member>|undefined;
  user_id: string;
  user_valid = false;
  room_id: string;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private roomService: RoomsService,
    public usersService: UsersService) {
    const params = this.route.snapshot.paramMap;
    this.room_id = String(params.get('room_id'));
    this.user_id = "";
    this.messages = roomService.getMessagesCollection(this.room_id, ref => ref.orderBy('sent')).valueChanges();
    this.members = roomService.getMembersCollection(this.room_id).valueChanges();
    this.members.subscribe((result) => {
      this.messages = roomService.getMessagesCollection(this.room_id, ref => ref.orderBy('sent')).valueChanges();
      this.membersMap = new Map<string, Member>(result.map(i => [i.user_id, i]));
    });
  }

  ngOnInit(): void {
    $("#menu-toggle").on('click', function (e) {
      e.preventDefault();
      $("#members-list").toggleClass("collapsed");
      $("#grid-container").toggleClass("collapsed");
    });
  }

  ngAfterViewInit(): void {
    this.roomService.getRoom(this.room_id).valueChanges().subscribe((result) => {
      this.room = result;
      this.user_id = this.authService.userData.uid;
      this.user_valid = this.validateUserPass();
    });
  }

  validateUserPass(pass?: string | undefined): boolean {
    // If room is undefined, does nothing
    if (this.room === undefined) {
      console.log("Room is undefined");
      return false;
    }
    // Room has no password: everyone can enter
    if (this.room.password.length == 0) {
      console.log("room has no password");
      this.roomService.addMember(this.room_id, this.user_id);
      return true;
    }
    // No password was given: only enter who was already in
    if (pass === undefined) {
      if (this.room.owner == this.user_id) {
        console.log("user is the owner");
        this.roomService.addMember(this.room_id, this.user_id);
        return true;
      }
      var is_member = this.membersMap ? this.membersMap.has(this.user_id) : false;
      console.log(`user is already member: ${is_member}`);
      return is_member;
    }
    // Password was given: check room password
    console.log("verifying password");
    if (pass !== undefined && this.room.password === pass) {
      this.roomService.addMember(this.room_id, this.user_id, pass);
      return true;
    }
    return false;
  }

  sendMessage(text: string) {
    if (text.length > 0) {
      this.roomService.sendMessage(
        this.room_id, this.authService.userData.uid, text);
      $("#entry").val("");
    }
  }

  rollDice(dices: string) {
    let re_dice = /#(\d+)D(\d+)/;
    let match = re_dice.exec(dices);
    if (match !== null) {
      let quantity = Number.parseInt(match[1]);
      let faces = Number.parseInt(match[2]);
      let results = [];
      for (let d = 0; d < quantity; d++) {
        results.push(Math.floor(Math.random() * faces) + 1);
      }
      var highers = results.reduce(this.Max2Reducer, []);
      var sum = highers.reduce((acc, cur) => acc + cur);
      let message = `${dices}: ${results.join(', ')} → ${highers.join('+')} = ${sum}`;
      this.roomService.sendMessage(
        this.room_id, this.authService.userData.uid, message
      );
    }
  }

  Max2Reducer(acc: number[], cur: number): number[] {
    if (acc.length == 0) {
      return [cur];
    } else if (acc.length == 1) {
      return acc.concat([cur]);
    } else {
      const max = Math.max(...acc);
      const min = Math.min(...acc);
      return (cur >= max || cur > min) ? [max, cur] : acc;
    }
  }
}
