import { Component, OnInit } from '@angular/core';
import { AuthService } from './shared/services/auth.service';
import * as $ from "jquery";
import { Dropdown } from 'bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'Dice Rooms';
  display_name = ""

  constructor (public authService: AuthService) {
    authService.fireauth.authState.subscribe((user) => {
      if (user !== null) {
        if (user.displayName !== null) {
          this.display_name = user.displayName;
        } else if (user.uid !== null) {
          this.display_name = "Anonymous";
        }
      } else {
        this.display_name = "";
      }
    });

  }

  ngOnInit(): void {
    new Dropdown($("#usermenu")[0]);
  }
}