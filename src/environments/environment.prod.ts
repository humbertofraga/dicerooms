export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "{{APIKEY}}",
    authDomain: "dice-rooms.firebaseapp.com",
    projectId: "dice-rooms",
    storageBucket: "dice-rooms.appspot.com",
    messagingSenderId: "{{MESSAGINGSENDERID}}",
    appId: "{{APPID}}"
  }
};
